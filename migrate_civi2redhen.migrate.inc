<?php
/**
 * @file
 * Base class
 */

abstract class CiviMigration extends DynamicMigration {
  /**
   * Connection key for the DatabaseConnection holding the source Drupal
   * installation.
   *
   * @var
   */
  protected $sourceConnection;

  /**
   * The major version of the Drupal install serving as the migration
   * source, e.g. '6'.
   *
   * @var int
   */
  protected $sourceVersion;

  /**
   * Options to be passed to source constructors.
   *
   * @var array
   */
  protected $sourceOptions = array();

  /**
   * An array of available source fields, beyond those in the base query.
   * Derived classes should populate this before calling the parent
   * constructor.
   *
   * @var array
   */
  protected $sourceFields = array();

  /**
   * Source type (bundle), if any.
   *
   * @var string
   */
  protected $sourceType = '';

  /**
   * Set to TRUE to suppress highwater marks or track_changes (i.e., to only
   * import new items, skipping updated items, on subsequent imports).
   *
   * @var bool
   */
  protected $newOnly = FALSE;

  /**
   * Required arguments:
   *
   * source_connection - Connection key for the DatabaseConnection holding the
   *  source Drupal installation.
   * source_version - Major version number (as an integer) of the source install.
   * machine_name - Machine name under which a particular migration is registered.
   * description - Description of the migration.
   * group_name - The group (import job) containing this migration (import
   *  task).
   *
   * Optional arguments:
   *
   * source_database - Array describing the source connection, to be defined in
   *  the constructor. If absent, the source connection is assumed to be established
   *  elsewhere (typically settings.php).
   * group - Migration group to add this migration to.
   * dependencies - Array of migrations that must be run before this one.
   * soft_dependencies - Array of migrations that should be listed before this one.
   * format_mappings - Array keyed by source format IDs or machine names, with
   *  the values being the corresponding D7 machine name. If unspecified,
   * source_options - Array to be passed as options to source constructors,
   *  overriding the defaults (map_joinable FALSE, cache_counts TRUE, cache_key
   *  derived from the machine name).
   * version_class - The name of a custom DrupalVersion class overriding the
   *  default derived from source_version.
   * new_only - For any destination types that support highwater marks or
   *  track_changes, suppress that support so repeated migrations only import
   *  new items.
   *
   * @param array $arguments
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->sourceVersion = $this->arguments['source_version'];
    if (isset($this->arguments['version_class'])) {
      $version_class = $this->arguments['version_class'];
    }
    else {
      $version_class = 'DrupalVersion' . $this->sourceVersion;
    }
    $this->version = new $version_class($this->arguments);
    if (!isset($this->arguments['group'])) {
      $this->arguments['group'] = MigrateGroup::getInstance($this->arguments['group_name']);
    }
    elseif (!is_object($this->arguments['group'])) {
      $this->arguments['group'] = MigrateGroup::getInstance($this->arguments['group']);
    }
    $this->sourceConnection = $this->arguments['source_connection'];
    if (!empty($this->arguments['source_database'])) {
      Database::addConnectionInfo($this->sourceConnection, 'default',
                                  $this->arguments['source_database']);
    }
    if (!empty($this->arguments['source_type'])) {
      $this->sourceType = $this->arguments['source_type'];
    }
    $this->description = $this->arguments['description'];
    if (!empty($this->arguments['dependencies'])) {
      $this->dependencies = $this->arguments['dependencies'];
    }
    if (!empty($this->arguments['soft_dependencies'])) {
      $this->softDependencies = $this->arguments['soft_dependencies'];
    }
    $this->sourceOptions = array('map_joinable' => FALSE, 'cache_counts' => TRUE,
                     'cache_key' => 'migrate_' . $this->machineName);
    if (!empty($this->arguments['source_options'])) {
      $this->sourceOptions = array_merge($this->sourceOptions,
                                         $this->arguments['source_options']);
    }

    if (!empty($this->arguments['format_mappings'])) {
      $this->formatMappings = $this->arguments['format_mappings'];
    }
    else {
      $this->formatMappings = $this->version->getDefaultFormatMappings();
    }

    if (!empty($this->arguments['new_only'])) {
      $this->newOnly = $this->arguments['new_only'];
    }
  }

}

/*
 * RedHen Contact Migration class
 */
class CiviMigrationContacts extends CiviMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    // TODO: Good example of extending MigrateDestinationEntity: asset/includes/asset.migrate.inc
    // TODO: Using given mapping migrate Civi Contacts to RedHen Contacts
  }
}

/*
 * RedHen Organization Migration class
 */
class CiviMigrationOrgs extends CiviMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    // TODO: Using given mapping migrate Civi Contacts to RedHen Organizations
  }
}