<?php

/**
 * @file
 * Configuration page for creating and modifying a mapping.
 */

/**
 * Return a form for a CiviCRM2RedhenMigrationMapping entity.
 */
function migrate_civi2redhen_mapping_form($form, &$form_state, CiviCRM2RedhenMigrationMapping $mapping = NULL, $op = 'edit') {
  if ($op == 'clone') {
    $mapping->label .= ' (cloned)';
    $mapping->name = '';
  }
  $form['#id'] = 'civcrm_to_redhen_migration_mapping_form';
  if (!isset($form_state['civcrm_to_redhen_migration_mapping'])) {
    $form_state['civcrm_to_redhen_migration_mapping'] = $mapping;
  }


  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#value' => t('Save mapping'),
    '#type' => 'submit',
  );

  return $form;
}



/**
 * Validate callback for migrate_civi2redhen_mapping_form().
 */
function functionmigrate_civi2redhen_mapping_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  return TRUE;
}

/**
 * Submit handler for migrate_civi2redhen_mapping_form().
 */
function migrate_civi2redhen_mapping_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  salesforce_set_message(t('CiviCRM to RedHen field mapping saved.'));
  $form_state['redirect'] = 'admin/content/civi_to_redhen_migrate/mappings';
}

