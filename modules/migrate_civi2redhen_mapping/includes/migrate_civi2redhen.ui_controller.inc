<?php

/**
 * @file
 * Contains CiviCRM2RedhenMigrationMappingUIController.
 */

/**
 * CiviCRM2RedhenMigration Mapping UI controller.
 */
class CiviCRM2RedhenMigrationMappingUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   *
   * Ignore that code sniffer wants hook_menu() to be in lowerCamelCase.
   */
  // @codingStandardsIgnoreStart
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage CiviCRM to RedHen Migration mappings';
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    return $items;
  }
  // @codingStandardsIgnoreEnd
  // TODO: Complete Mapping UI Controller
}
