<?php

/**
 * @file
 * Contains CiviCRM2RedhenMigrationMapping.
 */

/**
 * Entity class for CiviCRM2RedhenMigrationMapping
 */
class CiviCRM2RedhenMigrationMapping extends Entity {

  // Only one bundle type for now.
  public $type = 'migrate_civi2redhen_mapping';

  /**
   * Constructor for CiviCRM2RedhenMigrationMapping.
   *
   * @param array $values
   *   Associated array of values for the fields the entity should start with.
   */
  public function __construct(array $values = array()) {
    parent::__construct($values, 'migrate_civi2redhen_mapping');
  }

  /**
   * Save the entity.
   *
   * @return object
   *   The newly saved version of the entity.
   */
  public function save() {
    $this->updated = REQUEST_TIME;
    if (isset($this->is_new) && $this->is_new) {
      $this->created = REQUEST_TIME;
    }
    return parent::save();
  }

  /**
   * Retreive the default URI.
   *
   * @return array
   *   Associated array with the default URI on the 'path' key.
   */
  protected function defaultUri() {
    return array('path' => 'admin/structure/salesforce/mappings/manage/' . $this->identifier());
  }

}
